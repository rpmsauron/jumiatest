
# Phone Numbers (Jumia Test)

## Intro
This project consists on a Laravel/PHP-built single-page application to display a simple list of phone number validations, in paginated form. 
There are two filters to apply to the list: valid-invalid phone numbers, and by-country.

It was developed on PHP version 7.4.5, with the Laravel framework version 7.14.1, over Sqlite3.

## Background ##
This project serves as a technical task for proficiency / expertise assessment for consultancy admission into a project by Jumia.
It can be built locally on a localhost.

The source file with the problem / requirements is [here](https://bitbucket.org/rpmsauron/jumiatest/src/master/source%20files/Jumiamarket-recruitmenttest.pdf)
and in the same folder a sample file with the database data to follow up from.

## Interpretation ##
As no reference to where the Countries in this application "exist", I took the decision to model them into the domain as a model storing in a field its inherent regex that validates its phone numbers.
From the sample data provided, it was assumed that a number in the database will always have the country code separated from the canonical number with a space, and that is what tokenizes the number into both parcels.

**Note:** As I had never worked with Sqlite3 before, I am not aware of exactly what the .db format of the provided file is or how to work with it, so a .sqlite file (database/database.sqlite) is the souce database file in use by the application, created from that .db file provided.
The model simply consists of two tables: **countries** and **customers**. A customer's phone number is validated against a country's pattern via regex matching.
It was interpreted as well that both filters should work in a composite manner, i.e.: they should work together, one applied to the other.

## Local install ##
#### Requirements: ####
To use it locally, a webserver such as Nginx or Apache (I use [valet](https://laravel.com/docs/8.x/valet) for Mac to automate creation of localhosts, embedded with Nginx and PHP), PHP 7.4, and Sqlite3 is required.

To install it, get it first from the repository:
```
git clone git@bitbucket.org:rpmsauron/jumiatest.git
```
then switch into the project root directory and run:
```
composer install
```
to install/fetch the vendor folder with subsequent dependencies.

Copy the .env_example file at the root directory into a .env file and adapt the values as you need, paying special attention to the database configuration that must match your local database settings:
```
DB_CONNECTION=sqlite
DB_DATABASE=<absolute_path_to_project_folder>/database/database.sqlite
```

Run the migrations to ensure a basic database for usage, and generate seed data (which in this case is the default examples of Countries provided in the example):
```
php artisan migrate --seed
```
Simply access the hostname now and you should see the UI of the SPA.

## Relevant source files for development ##
The frontend as simple as it is, resides mostly on two files:

[resources/views/welcome.blade.php](https://bitbucket.org/rpmsauron/jumiatest/src/master/resources/views/welcome.blade.php)

[resources/views/customers/index.blade.php](https://bitbucket.org/rpmsauron/jumiatest/src/master/resources/views/customers/index.blade.php)


The route definition is at:

[routes/web.php](https://bitbucket.org/rpmsauron/jumiatest/src/master/routes/web.php)

The models are:

[app/Country.php](https://bitbucket.org/rpmsauron/jumiatest/src/master/app/Country.php)

[app/Customer.php](https://bitbucket.org/rpmsauron/jumiatest/src/master/app/Customer.php)


The database migrations are at:

[database/migrations](https://bitbucket.org/rpmsauron/jumiatest/src/master/database/migrations/)


and the Countries being generated from:

[database/seeds/CountrySeeder](https://bitbucket.org/rpmsauron/jumiatest/src/master/database/seeds/CountrySeeder.php)


The Controller for the main filter and display action of the SPA is:

[app/Http/Controllers/CustomerController.php](https://bitbucket.org/rpmsauron/jumiatest/src/master/app/Http/Controllers/CustomerController.php)




