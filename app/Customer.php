<?php

namespace App;

use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $table = 'customer';

    /**
     * Gets the country code.
     * Resolution for phone tokens requires a space to separate country code from 
     * canonical phone number.
     *
     * @return string The country code.
     */
    public function getCountryCode() :string
    {
        $tokens = explode(" ", $this->attributes["phone"]);

        return preg_replace('/[()]/', '', $tokens[0]);
    }

    /**
     * Gets the canonical phone number for this Customer, sans the country code.
     * Resolution for phone tokens requires a space to separate country code from 
     * canonical phone number.
     *
     * @return string|null The canonical phone number for this Customer, or null 
     *                     if international phone number notation cannot be resolved 
     *                     into country code and canonical phone number.
     */
    public function getPhone() :?string
    {
        $tokens = explode(" ", $this->attributes["phone"]);
        
        return $tokens[1] ?? null;
    }

    /**
     * Gets the Country from this Customer's country code, or null if none exists.
     * There should not be two countries simultaneously matching one code, so it 
     * simply returns the first one found matching.
     *
     * @return Country|null The Country maytching the country code of this Customer.
     */
    public function getCountryFromCountryCode() :?Country
    {
        return Country::where("country_code", "=", $this->getCountryCode())
                ->first();
    }

    /**
     * Gets the validity state for this Customer's phone number.
     * It returns 1 for a valid number and 0 for an invalid number.
     *
     * @return int The validity state for this Customer's phone number.       
     */
    public function getState() :int
    {
        $country = $this->getCountryFromCountryCode();

        if (!$country) {
            return false;
        }

        return preg_match("/$country->regex/", $this->attributes["phone"]);
    }

    /**
     * Gets all Customers with a valid or invalid phone number.
     * If $valid is true, gets Customers with valid phone numbers, if it's false,
     * gets Customers with invalid phone numbers.
     * A phone number is valid if, given its country code, it matches the exact 
     * complete regular expression.
     *
     * @param  bool       $valid The validity state to get Customers for.
     *
     * @return Collection A Collection of all Customers with valid phone numbers.
     */
    public static function getAllCustomersForState($valid = true) :Collection
    {
        $customers = [];
        $countries      = Country::all();

        foreach ($countries as $country) {
                $customers = array_merge($customers, $country->getCustomersForState($valid)->all());
        }

        return collect($customers);
    }
}
