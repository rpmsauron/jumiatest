<?php

namespace App\Http\Controllers;

use App\Country;
use App\Customer;
use Illuminate\View\View;

class CustomerController extends Controller
{
    /**
     * Controller action endpoint for route 'home'.
     *
     * @return View The view Response for this endpoint.
     */
    public function index() :View
    {
        $perPage         = config("params.pagination");
        $filterValid     = request()->get("valid");
        $filterCountryId = request()->get("country") != "" ? request()->get("country") : null;

        // If any filter is in use, check which are and process each filter value.
        if ($filterValid || $filterCountryId) {
            // If both filters are used.
            if ($filterValid && $filterCountryId) {
                $filterCountry = Country::find($filterCountryId);
                if ($filterCountry) {
                    // Retrieve valid and invalid, by Country.
                    if ($filterValid == 0) {
                        $customers = $filterCountry->getCustomers()
                                   ->paginate($perPage);
                    } 
                    // Retrieve valid, by Country.
                    elseif ($filterValid == 1) {
                        $customers = $filterCountry->getCustomersForState()
                                   ->paginate($perPage);
                    } 
                    // Retrieve invalid, by Country.
                    elseif ($filterValid == 2) {
                        $customers = $filterCountry->getCustomersForState(false)
                                   ->paginate($perPage);
                    }
                }
            } 
            
            // If only validity filter is used.
            elseif ($filterValid) {
                // Retrieve all valid and invalid.
                if ($filterValid == 0) {
                    $customers = Customer::paginate(config("params.pagination"));
                }
                // Retrieve all valid.
                elseif ($filterValid == 1) {
                    $customers = Customer::getAllCustomersForState()
                                 ->paginate($perPage);
                }
                // Retrieve all invalid.
                elseif ($filterValid == 2) {
                    $customers = Customer::getAllCustomersForState(false)
                                 ->paginate($perPage);
                }
            } 

            // If only Country filter in used.
            elseif ($filterCountryId) {
                $filterCountry = Country::find($filterCountryId);
                if ($filterCountry) {
                    $customers = $filterCountry->getCustomers()
                                 ->paginate($perPage);
                }
            }
        } 

        // Default fallback: if no filter is in use, show all Customers' phone numbers.
        if (!isset($customers)) {
            $customers = Customer::paginate($perPage);
        }        

        $countries = Country::all();

        return view("customers.index", compact("customers", "countries", "filterCountryId"));
    }
}
