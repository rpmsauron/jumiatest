<?php

namespace App\Providers;

use Illuminate\Support\Collection; 
use Illuminate\Pagination\Paginator; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Enable pagination
         * taken from:
         * https://stackoverflow.com/questions/30420505/how-can-i-paginate-a-merged-collection-in-laravel-5
         */
        if (!Collection::hasMacro('paginate')) {
            Collection::macro('paginate', 
                function ($perPage = 5, $page = null, $options = []) 
                {
                    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                    return (new LengthAwarePaginator(
                        $this->forPage($page, $perPage)->values()->all(), $this->count(), $perPage, $page, $options))
                        ->withPath('');
                }
            );
        }
    }
}
