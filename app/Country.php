<?php

namespace App;

use App\Customer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Country extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $table = 'countries';

    /**
     * Gets a Query Builder to get all Customers whose phone number matches this 
     * Country's phone number pattern.
     *
     * @return Builder The Query Builder for getting the Customers.
     */
    public function getCustomers() :Builder
    {
        return Customer::where("phone", "LIKE", "(" . $this->attributes['country_code'] . ") %");
    }

    /**
     * Gets all Customers with valid phone numbers or invalid phone numbers, for 
     * this Country.
     * If $valid is true, gets Customers with valid phone numbers, if it's false,
     * gets Customers with invalid phone numbers.
     * A phone number is valid if, given its country code, it matches the exact 
     * complete regular expression.
     *
     * @param  bool       $valid The validity state to get Customers for.
     *
     * @return Collection A Collection of all Customers with valid phone numbers.
     */
    public function getCustomersForState($valid = true) :Collection
    {
        /**
         * Note: regex-matching is done in PHP as it seems SQL's Regex operator is 
         * not supported on Sqlite3.
         */
        $matchingCustomers = [];
        $allCustomers      = $this->getCustomers()->get();

        // Gets Customers with a valid phone number.
        if ($valid) {
            foreach ($allCustomers as $customer) {
                if (preg_match("/" . $this->attributes['regex'] . "/", $customer->phone)) {
                    array_push($matchingCustomers, $customer);
                }
            }
        }
        // Gets Customers with an invalid phone number.
        else {
            foreach ($allCustomers as $customer) {
                if (!preg_match("/" . $this->attributes['regex'] . "/", $customer->phone)) {
                    array_push($matchingCustomers, $customer);
                }
            }
        }

        return collect($matchingCustomers);
    }
}
