<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::truncate();

        Country::create([
            "name"         => "Cameroon",
            "country_code" => "237",
            "regex"        => "\(237\)\ ?[2368]\d{7,8}$"
        ]);

        Country::create([
            "name"         => "Ethiopia",
            "country_code" => "251",
            "regex"        => "\(251\)\ ?[1-59]\d{8}$"
        ]);

        Country::create([
            "name"         => "Morocco",
            "country_code" => "212",
            "regex"        => "\(212\)\ ?[5-9]\d{8}$"
        ]);

        Country::create([
            "name"         => "Mozambique",
            "country_code" => "258",
            "regex"        => "\(258\)\ ?[28]\d{7,8}$"
        ]);

        Country::create([
            "name"         => "Uganda",
            "country_code" => "256",
            "regex"        => "\(256\)\ ?\d{9}$"
        ]);
    }
}
