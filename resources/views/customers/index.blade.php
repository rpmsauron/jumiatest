@extends('welcome')

@section('content')
    <div class="card">    
        <div class="card-header">
            <div>
                <form method="GET"
                    action="{{ route('home') }}"
                    id="filter-customers" >
                    @csrf

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="country"
                                    id="country"
                                    class="form-control"
                                    onchange="document.getElementById('filter-customers').submit()">
                                    <option value="" 
                                        @if(app('request')->input('country') == null)
                                            selected 
                                        @endif
                                        >
                                        All
                                    </option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}"
                                        @if($filterCountryId == $country->id )
                                            selected
                                        @endif
                                     >
                                        {{ $country->name }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="valid"
                                    id="valid"
                                    class="form-control"
                                    onchange="document.getElementById('filter-customers').submit()">
                                    <option value="0"
                                    @if(app('request')->input('valid') == 0)
                                        selected
                                    @endif
                                    >All phone numbers</option>
                                    <option value="1"
                                        @if(app('request')->input('valid') == 1)
                                            selected
                                        @endif
                                    >Valid phone numbers</option>
                                    <option value="2"
                                        @if(app('request')->input('valid') == 2)
                                            selected
                                        @endif
                                    >Invalid phone numbers</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card-body">
            <table width="70%" 
                class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Country</th>
                        <th scope="col">State</th>
                        <th scope="col">Country code</th>
                        <th scope="col">Phone num.</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!isset($customers))
                        @php
                            $customers = App\Customer::paginate(config("params.paginate"));
                        @endphp
                    @endif
                    @foreach($customers as $customer)
                        <tr>
                            <td>{{ is_null($customer->getCountryFromCountryCode()) ? "N/A" : $customer->getCountryFromCountryCode()->name }}</td>
                            <td>{{ $customer->getState() == 1 ? "OK" : "NOK" }}</td>
                            <td>+{{ $customer->getCountryCode() }}</td>
                            <td>{{ $customer->getPhone() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        @if($customers->hasPages())
            <div class="card-footer">
                {{ $customers->appends(['valid' => request()->query('valid'), 'country' => request()->query('country')])->links() }}
            </div>
        @endif
    </div>
@endsection

