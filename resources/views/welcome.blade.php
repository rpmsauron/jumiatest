<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" 
            content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Jumia Test') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" 
            rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        
        <link href="{{ asset('css/app.css') }}" 
            rel="stylesheet">
        <link href="{{ asset('css/datatables.css') }}" 
            rel="stylesheet">
        
    </head>
    <body>
        <div class="full-height">
            <div class="mt-5 ml-5 mr-5 content" id="main">
                <div class="title m-b-md">
                    <h2>Phone Numbers (Jumia Test)</h2>
                </div>
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
            
            <div id="footer" class="ml-5">
                Developed by Rui Silva, 2021
            </div>
        </div>
       
        
        @yield('scripts')
        
        <script src="{{ asset('js/app.js') }}"></script> 
        <script src="{{ asset('js/datatables.js') }}"></script>
    </body>
</html>
